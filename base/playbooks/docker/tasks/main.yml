---
- name: Install list of Packages
  apt: name={{ item }} state=present autoremove=yes
  loop:
       - apt-transport-https
       - ca-certificates
       - curl
       - software-properties-common
       - python3
       - python3-pip
       - python3-dev
       - build-essential
       - python3-setuptools
   
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu"
  tags:
     - installation

- name: Install list of Packages
  pip: name=docker-py
  tags:
     - installation docker-py

- name: Install list of Packages
  yum: name={{ item }} state=installed
  loop:
       - yum-utils
       - device-mapper-persistent-data
       - lvm2
       - update-motd
  when: ansible_os_family == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == "Amazon"

- name: Add Docker GPG key
  apt_key: url=https://download.docker.com/linux/ubuntu/gpg
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu"
  tags:
    - load_repo

- name: Append Docker Apt Repo
  apt_repository:
     repo: "deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ansible_distribution_release}} stable"
     state: present
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu"
  tags:
      - load_repo

- name: Append Docker RedHat Repo
  copy:
    src: '{{ src_varios_dir }}/{{ src_docker_repo_file }}'
    dest: '{{ dst_yum_repo_dir }}'
    owner: root
    group: root
    mode: "u=rw,g=r,o=r"
  when: ansible_os_family == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == "Amazon"
  tags:
     - load_repo

- name: Updating Packages
  yum: update_cache=yes
  when: ansible_os_family == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == "Amazon"
  tags:
     - installation

- name: Updating Packages
  apt: update_cache=yes
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu"
  tags:
     - installation

- name: Install Docker Package
  apt: name={{ item }} state=present autoremove=yes allow_unauthenticated=yes
  loop:
      - docker-ce
      - docker-compose
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu"
  tags:
     - installation

- name: Install Docker Package
  yum: name={{ item }} state=latest
  loop:
      - docker
  when: ansible_os_family == 'RedHat' or ansible_distribution == 'CentOS' or ansible_distribution == "Amazon"
  tags:
     - installation

- name: Copy Docker Daemon File
  copy:
       src: '{{ src_docker_dir }}/{{ src_docker_conf_file }}'
       dest: '{{ dst_docker_conf_dir }}'
       owner: root
       group: root
       mode: "u=rw,g=r,o=r"
  tags:
     - load_daemon

- name: Use Docker as Non-Root User
  command: "/usr/sbin/usermod -aG docker {{ item.name }} "
  loop: "{{ users }}"
  tags:
    - add_dock_non-root

- name: Restarting docker service
  service: name={{ item }} state=restarted
  loop:
      - docker
  when: ansible_os_family == 'Debian' or ansible_distribution == "Ubuntu" or ansible_distribution == "Amazon"
  tags:
     - docker_restart

- name: Enable docker service
  systemd: name={{ item }} state=enable
  loop:
      - docker
  when: ansible_distribution == 'CentOS'
  tags:
     - docker_restart

- name: Restarting docker service
  systemd: name={{ item }} state=restarted
  loop:
      - docker
  when: ansible_distribution == 'CentOS'
  tags:
     - docker_restart

- name: Copying Docker Compose file
  copy:
    src: '{{ src_docker_dir }}/{{ docker_compose_file }}'
    dest: '{{ dst_docker_compose_dir }}'
  tags:
     - copying_docker_compose_file

- name: Run Docker Compose
  docker_compose:
    project_src: '{{ dst_docker_compose_dir }}'
    files: '{{ docker_compose_file }}'
    state: present
    build: yes
  when:  with_compose is defined and with_compose == 'true' and with_compose != 'none'
  tags:
    - run_docker_compose
