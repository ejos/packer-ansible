![](https://pbs.twimg.com/media/C8mkdvHXUAIzX2h.jpg)
# Packer

Do you know what packer is? Click here [Packer](https://www.packer.io/)

## Why you should must use packer? 
Short answer 'cause you can build you own custom images either for AWS AMI, Google Image or whatever you need Base on Ubuntu, Amazon Linux images, Google, Azure or so on

## How to Install Packer

* Download binary form here [Download](https://www.packer.io/downloads.html)
* If you're using unix based OS place the binary on your PATH, if not you are a disgusting person.

## How to use it?

In order to build AMI or Google Image remember to setup your AWS or Google credentials, follow this [link](http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-quick-configuration) for AWS for example.


## Variables that you must setup

* [users.yml:](base/playbooks/vars/users.yml)
  - name: johnDoe
    key: '/tmp/files/johndoe.pub'

* [docker.yml:](base/playbooks/vars/docker.yml)
  - name: johnDoe

'Cause this is a Proof of Concept, you must setup your own [east.json](base/playbooks/vars/east.json) configuration as well as the rest of the varible listed for upload your custom images ones you builded

## About this project
```
.
├── base
│   ├── aws.json
│   ├── gcloud.json
│   └── playbooks
│       ├── base.yml
│       ├── docker
│       │   └── tasks
│       │       └── main.yml
│       ├── files
│       │   ├── 02-landscape-sysinfo
│       │   ├── daemon.json
│       │   ├── docker-compose.yml
│       │   ├── johndoe.pub
│       │   ├── motd.sh
│       │   ├── sources.list
│       │   ├── ubuntu_sshd
│       │   └── users.j2
│       ├── users
│       │   └── tasks
│       │       └── main.yml
│       └── vars
│           ├── aws-east.json
│           ├── docker.yml
│           ├── gcloud-east.json
│           └── users.yml
└── README.md



```

Packer can be provisioned with shell script, ansible, chef... some of the directories uses combination of shell and ansible, you can use whatever you want. 

## Working with Packer

Packer requires a json file with a structure like [this](https://www.packer.io/docs/templates/index.html) once you have build or using the one of the ami directories you can check de template with the following command:

```
  packer validate  _cloud_.json
```

if your are storing variables on another file:

```
packer validate -var-file=variables/_cloud_.json _cloud_.json

```

If is a valid template you can build the image and by the way you dont need to specify ssh key on ami, paker will create a temporary key and use it and it will delete the key... cool right?

## Exec packer 

```
packer build _cloud_.json

```

Or if you are using **Variables**, you must use:

```
packer build -var-file=variables/_cloud_.json _cloud_.json 
```
